"""
File load utilities.
"""

def opf(path):
    """Opens a path and returns the result."""
    try:
        a_file = open(path)
        return a_file
    except Exception:
        raise FileNotFoundError('oopsie woopsie')
