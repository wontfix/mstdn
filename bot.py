"""hi this is a mastodon bot"""

from mastodon import Mastodon
from ruamel.yaml import YAML
import sys

try:
    __ = YAML(typ='safe').load(open('config.yaml'))
except:
    print('Error: config.yaml not found.')
    sys.exit(127)

CONF = {
    'id': __['client_id'],
    'secret': __['client_secret'],
    'token': __['access_token'],
    'instance': __['instance'] or 'botsin.space'
}


def launch():
    client = Mastodon(
        client_id=CONF['id'],
        client_secret=CONF['secret'],
        access_token=CONF['token'],
        api_base_url=CONF['instance'])
    return client

bot = launch()

print('Welcome to the mstdn interpreter.')
print('For basic help, type `help`.')
print('To exit, type `quit`.')

while True:
    try:
        command = input('>>> ')
        if command == 'help':
            print('Commands:')
            print('toot <text> - Makes a toot.')
            print('quit - Exits the interpreter.')
            print('help - Shows this text.')
        elif command == 'quit':
            print('Goodbye.')
            sys.exit()
        elif command[0:4] == 'toot':
            try:
                thing = command.split(' ')[1]
            except IndexError:
                print('Invalid arguments.')
                print('Usage: toot <text>')
            else:
                print('Tooting...')
                content = command.replace('toot', '')
                t = bot.toot(content)
                print(f'Sent. URL: {t["url"]}')
        else:
            print('Unknown command.')
    except (EOFError, KeyboardInterrupt):
        print('\nExiting.')
        sys.exit()
        
